package fr.cnam.foad.nfa035.fileutils.simpleaccess.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {

    @Override
    public String serialize(File image) throws IOException {
        byte[] bytes = Files.readAllBytes(image.toPath());
        String encodeToString = Base64.getEncoder().encodeToString(bytes);
        return encodeToString;
    }

    @Override
    public byte[] deserialize(String encodedImage) {
        return Base64.getDecoder().decode(encodedImage);
    }
}
